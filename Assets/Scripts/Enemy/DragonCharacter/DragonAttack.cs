using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class DragonAttack : MonoBehaviour
{
	[Header("# VFX Fire Breathe Object")]
	public VisualEffect m_FireBreathe;



	public bool isSpreadFire { get; private set; }

	public bool isClawAttack { get; private set; }

	public void OnSpreadFireStarted()
	{
		isSpreadFire = true;
		m_FireBreathe.SetBool("Enable", true);
		m_FireBreathe.SetFloat ("FloorHight", transform.position.y);
	}

	public void OnSpreadFireFinished()
	{
		if (isSpreadFire)
		{
			isSpreadFire = false;
			m_FireBreathe.SetBool("Enable", false);
		}
	}

	public void OnClawAttackStarted()
	{
		isClawAttack = true;
	}

	public void OnClawAttackFinished()
	{
		if (isClawAttack) isClawAttack = false;
	}
}
