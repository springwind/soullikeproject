using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.UI.GridLayoutGroup;

public sealed class DragonMovement : MonoBehaviour
{
	private DragonCharacter _DragonCharacter;

	/// <summary>
	/// 영역의 중앙 위치를 나타냅니다.
	/// </summary>
	private Vector3 _AreaCenter;

	/// <summary>
	/// 영역의 반지름을 나타냅니다.
	/// </summary>
	private float _AreaRadius;

	[Header("# Yaw 회전 속력")]
	public float m_TurnSpeed = 15.0f;

	/// <summary>
	/// 뒤로 대시중임을 나타냅니다.
	/// </summary>
	public bool isDashBackward { get; private set; }

	/// <summary>
	/// 현재 회전중임을 나타냅니다.
	/// </summary>
	public bool isTurning { get; private set; }

	/// <summary>
	/// 목표 Yaw 회전값을 나타냅니다.
	/// isTurning 가 True 인 경우에 회전됩니다.
	/// </summary>
	public float targetYawAngle { get; set; }

	private void Awake()
	{
		_DragonCharacter = GetComponent<DragonCharacter>();
		_DragonCharacter.agent.angularSpeed = 0.0f;
	}

	private void Update()
	{
		if (isTurning) TurnYaw();

		if (isDashBackward) ClampDragonInDash();
	}

	private void TurnYaw()
	{
		// 현재 Yaw 회전값
		Quaternion currentAngle = transform.rotation;
		Quaternion targetAngle = Quaternion.Euler(Vector3.up * targetYawAngle);

		// 회전이 완료된 경우, 회전을 중단합니다.
		if ((int)currentAngle.eulerAngles.y == (int)targetAngle.eulerAngles.y)
		{
			transform.rotation = Quaternion.Euler(Vector3.up * targetYawAngle);
			isTurning = false;
		}

		// 목표 Yaw 회전으로 변경합니다.
		currentAngle = Quaternion.Lerp(currentAngle, targetAngle, m_TurnSpeed * Time.deltaTime);

		// 연산된 회전값을 적용합니다.
		transform.rotation = currentAngle;
	}

	private void ClampDragonInDash()
	{
		Vector3 currentPositionWithoutY = transform.position;
		currentPositionWithoutY.y = 0.0f;

		Vector3 areaCenterPositionWithoutY = _AreaCenter;
		areaCenterPositionWithoutY.y = 0.0f;

		// 영역 중앙과 Dragon 캐릭터 사이의 거리를 구합니다.
		float distance = Vector3.Distance(
			currentPositionWithoutY, areaCenterPositionWithoutY);

		// 영역을 벗어나는 경우 대시를 중단합니다.
		if (distance >= _AreaRadius)
		{
			Debug.Log("대시 중단");
			//FinishDashBackward();

			_DragonCharacter.agent.velocity = Vector3.zero;
			_DragonCharacter.agent.SetDestination(transform.position);
		}
	}

	public void StartYawTurning()
	{
		isTurning = true;
	}

	public void StartDashBackward()
	{
		isDashBackward = true;
		Vector3 direction = transform.forward * -1;
		float power = 20.0f;

		_DragonCharacter.agent.SetDestination(
			transform.position + direction * power);
		_DragonCharacter.agent.velocity = direction * power;
	}

	/// <summary>
	/// 이동 가능한 영역을 설정합니다.
	/// </summary>
	/// <param name="center"></param>
	/// <param name="radius"></param>
	public void SetMoveableArea(Vector3 center, float radius)
	{
		_AreaCenter = center;
		_AreaRadius = radius;
	}

	public void FinishDashBackward()
	{
		isDashBackward = false;

		_DragonCharacter.agent.SetDestination(transform.position);
	}

	public void DashForward(float power)
	{
		_DragonCharacter.agent.SetDestination(
			transform.position + transform.forward * power * 0.1f);
		_DragonCharacter.agent.velocity = transform.forward * power;
	}

}
