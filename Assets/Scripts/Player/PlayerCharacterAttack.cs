using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public sealed class PlayerCharacterAttack : MonoBehaviour
{
	private static AttackDataScriptableObject _AttackDataScriptableObject;

	[Header("# 방어 파티클 프리팹")]
	public VisualEffect m_BlockParticlePrefab;

	[Header("# 최대 공격 요청 개수")]
	public int m_MaxRequestAttackCount = 2;

	[Header("# Socket")]
	public Transform m_AttackAreaCheck_Start;
	public Transform m_AttackAreaCheck_End;

	[Header("# 적 레이어")]
	public LayerMask m_EnemyLayer;


	/// <summary>
	/// 이동 컴포넌트를 나타냅니다.
	/// </summary>
	private PlayerCharacterMovement _MovementComponent;

	/// <summary>
	/// 공격 영역 확인중임을 나타냅니다.
	/// </summary>
	private bool _AttackAreaChecking;

	/// <summary>
	/// 공격 처리된 GameObject 들을 임시로 저장해둘 리스트
	/// </summary>
	private List<GameObject> _DamagedGameObjects = new();

	/// <summary>
	/// 요청된 공격 데이터들을 담아둘 큐
	/// </summary>
	private Queue<AttackData> _RequestedAttackData = new();

	/// <summary>
	/// 진행중인 공격 데이터를 나타냅니다.
	/// </summary>
	private AttackData? _CurrentAttackData;


	/// <summary>
	/// 이전에 실행된 공격 이름
	/// </summary>
	private string _PrevAttackName;

	/// <summary>
	/// 목표 공격 콤보 카운트
	/// </summary>
	private int _TargetAttackComboCount;

	/// <summary>
	/// 공격 입력 확인 상태임을 나타냅니다.
	/// </summary>
	public bool isAttackInputChecking { get; private set; } = true;

	/// <summary>
	/// 현재까지 진행된 공격 콤보 카운트를 나타내는 프로퍼티입니다.
	/// </summary>
	public int currentAttackComboCount { get; private set; }

	/// <summary>
	/// 공격중임을 나타내는 프로퍼티입니다.
	/// </summary>
	public bool isAttack { get; private set; }

	/// <summary>
	/// 방어중임을 나타내는 프로퍼티입니다.
	/// </summary>
	public bool isBlocked { get; private set; }

	/// <summary>
	/// 공격력
	/// </summary>
	public float atk { get; set; }

	/// <summary>
	/// 방어 시작 시간
	/// </summary>
	public float guardStartTime { get; private set; }


	/// <summary>
	/// 공격 시작 시 발생하는 이벤트
	/// </summary>
	public event System.Action onAttackStarted;

	public event System.Action onAttackFinished;


	#region DEBUG
	private DrawGizmoSpheresInfo _AttackAreaCheckGizmoInfo;
	#endregion


	private void Awake()
	{
		if (!_AttackDataScriptableObject)
			_AttackDataScriptableObject = Resources.Load<AttackDataScriptableObject>("ScriptableObject/AttackData");

		_MovementComponent = GetComponent<PlayerCharacterMovement>();
	}
	private void Update() => AttackProcedure();

	private void FixedUpdate()
	{
		// 공격 영역을 확인하는 중이라면
		if (_AttackAreaChecking)
		{
			if (CheckAttackArea(out RaycastHit[] hitResults))
			{
				// 피해 적용
				ApplyDamage(hitResults);

				// 적 캐릭터 공격
				OnAttackEnemy(hitResults);
			}
		}
	}

	/// <summary>
	/// 피해를 적용합니다.
	/// </summary>
	/// <param name="hitResults"></param>
	private void ApplyDamage(RaycastHit[] hitResults)
	{
		if (!_CurrentAttackData.HasValue) return;

		foreach(RaycastHit hitResult in hitResults)
		{
			// 감지된 GameObject 객체
			GameObject hitGameObject = hitResult.transform.gameObject;

			// 이미 처리된 게임 오브젝트가 아닌 경우
			if (!_DamagedGameObjects.Contains(hitGameObject))
			{
				// 이미 처리된 오브젝트 리스트에 추가합니다.
				_DamagedGameObjects.Add(hitGameObject);

				// 피해량 계산
				float damage = atk * _CurrentAttackData.Value.atkMultiplier;

				IDamageInterface damagedObject = 
					hitResult.transform.GetComponent<IDamageInterface>();
				damagedObject?.OnApplyDamage(gameObject, damage);
			}
		}

	}

	/// <summary>
	/// 적 캐릭터를 공격했을 경우 호출됩니다.
	/// </summary>
	/// <param name="hitResults"></param>
	private void OnAttackEnemy(RaycastHit[] hitResults)
	{
		// 마지막으로 감지한 적 객체를 얻습니다.
		EnemyCharacter lastDamagedEnemy = null;
		foreach(RaycastHit hitResult in hitResults)
			lastDamagedEnemy = hitResult.transform.GetComponent<EnemyCharacter>() ?? lastDamagedEnemy;

		// 마지막으로 감지한 적 객체가 존재하지 않은 경우 함수 호출 종료.
		if (!lastDamagedEnemy) return;

		// Game UI 객체를 얻어 적 객체를 설정합니다.
		GameSceneInstance sceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();
		sceneInstance.m_GameUI.enemyHpUI.SetTargetEnemy(lastDamagedEnemy);
	}

	/// <summary>
	/// 공격 영역을 검사합니다.
	/// </summary>
	/// <param name="hitResult">감지 결과를 반환합니다.</param>
	/// <returns>감지 결과가 존재함을 반환합니다.</returns>
	private bool CheckAttackArea(out RaycastHit[] hitResults)
	{
		// 구체를 발사시킬 방향
		Vector3 checkDirection =
			(m_AttackAreaCheck_End.position - m_AttackAreaCheck_Start.position).normalized;

		// 검사할 거리
		float maxDistance = Vector3.Distance(
			m_AttackAreaCheck_End.position, m_AttackAreaCheck_Start.position);

		// 구체의 반지름
		float checkRadius = 0.4f;

		Ray ray = new Ray(m_AttackAreaCheck_Start.position, checkDirection);

		return PhysicsExt.SphereCastAll(
			out _AttackAreaCheckGizmoInfo,
			ray,
			out hitResults,
			checkRadius,
			maxDistance,
			m_EnemyLayer);
	}

	/// <summary>
	/// 요청된 공격을 처리합니다.
	/// </summary>
	private void AttackProcedure()
	{
		// 만약 이미 진행중인 공격이 존재하는 경우 실행 취소
		if (_CurrentAttackData != null) return;

		// 요청된 공격이 존재하지 않는 경우 실행 취소
		if (_RequestedAttackData.Count == 0) return;

		// 요청된 공격 데이터를 얻습니다.
		_CurrentAttackData = _RequestedAttackData.Dequeue();

		// 공격 진행
		isAttack = true;

		// 공격 처리됨 리스트를 비웁니다.
		_DamagedGameObjects.Clear();

		// 공격 시작 이벤트 발생
		onAttackStarted?.Invoke();

		// 콤보 증가
		if (currentAttackComboCount < _TargetAttackComboCount)
			++currentAttackComboCount;
	}

	/// <summary>
	/// 공격을 요청합니다.
	/// </summary>
	/// <param name="attackName"></param>
	public void RequestAttack(string attackName)
	{

		// 공격 입력을 확인하지 않는 상태라면 함수 호출 종료.
		if (!isAttackInputChecking) return;

		// 요청 가능 개수를 초과한 경우 함수 호출 종료.
		if (_RequestedAttackData.Count > m_MaxRequestAttackCount) return;

		// 공격 데이터를 얻습니다.
		AttackDataElem attackDataElem = _AttackDataScriptableObject.GetAttackDataFromName(attackName);

		// 공격 데이터를 찾지 못한 경우 함수 호출 종료.
		if (attackDataElem == null) return;

		// 사용할 공격 데이터를 얻습니다.
		AttackData attackData = attackDataElem.m_AttackData;


		// 같은 공격을 요청하는 경우
		if (_PrevAttackName == attackName)
		{
			// 진행시킬 수 있는 콤보가 남아있지 않은 경우 함수 호출 종료.
			if (attackData.maxComboCount == _TargetAttackComboCount) return;
		}

		// 공격 요청 큐에 담습니다.
		_RequestedAttackData.Enqueue(attackData);

		// 이전 공격 이름을 기록합니다.
		_PrevAttackName = attackName;

		// 목표 콤보를 증가시킵니다.
		if (attackData.maxComboCount > _TargetAttackComboCount)
			++_TargetAttackComboCount;
	}

	/// <summary>
	/// 가드를 요청합니다.
	/// </summary>
	public void StartGuard()
	{
		// 점프중이라면 함수 호출 종료
		if (!_MovementComponent.isGrounded) return;

		// 구르기 중이라면 함수 호출 종료
		if (_MovementComponent.isRolling) return;

		// 공격중이라면 함수 호출 종료
		if (isAttack) return;


		// 가드중 상태로 설정합니다.
		isBlocked = true;
		guardStartTime = Time.time;

		// 이동이 불가능하도록 합니다.
		_MovementComponent.AllowMovementInput(false);

		// 회전을 불가능하도록 합니다.
		_MovementComponent.AllowRotation(false);

		// 공격 입력을 블록시킵니다.
		StopAttackInputChecking();

		// 뷰 방향으로 회전시킵니다.
		_MovementComponent.LookAtCameraDirection();
	}

	public void FinishGuard()
	{
		// 가드중일 때에만 작동
		if (isBlocked)
		{
			// 가드 상태를 취소합니다.
			isBlocked = false;

			// 이동을 가능하도록 합니다.
			_MovementComponent.AllowMovementInput(true);

			// 회전을 가능하도록 합니다.
			_MovementComponent.AllowRotation(true);

			// 공격이 가능하도록 합니다.
			StartAttackInputChecking();
		}
	}



	/// <summary>
	/// 하나의 공격이 끝났을 경우 호출될 메서드입니다.
	/// </summary>
	public void OnAttackSequenceFinished()
	{
		// 다음 공격이 존재하지 않는 경우
		if (currentAttackComboCount == _TargetAttackComboCount)
		{
			// 공격 상태 비우기
			ClearAttackState();
		}

		// 다음으로 진행될 공격이 존재하는지 확인합니다.
		else _CurrentAttackData = null;
	}

	/// <summary>
	/// 공격 상태를 비웁니다.
	/// </summary>
	public void ClearAttackState()
	{
		// 콤보 카운트 초기화
		currentAttackComboCount = _TargetAttackComboCount = 0;

		// 현재 진행중인 공격 데이터 비우기
		_CurrentAttackData = null;

		// 공격 끝
		isAttack = false;

		// 예약된 공격 요청을 모두 비웁니다.
		_RequestedAttackData.Clear();

		// 공격 끝 이벤트 발생
		onAttackFinished?.Invoke();

		// 공격 입력을 받도록 합니다.
		StartAttackInputChecking();
	}

	/// <summary>
	/// 공격 입력 확인 시작
	/// </summary>
	public void StartAttackInputChecking() => isAttackInputChecking = true;

	/// <summary>
	/// 공격 입력 확인 중단
	/// </summary>
	public void StopAttackInputChecking() => isAttackInputChecking = false;

	/// <summary>
	/// 공격 영역 활성화 / 비활성화
	/// </summary>
	/// <param name="enable">활성화 여부 전달</param>
	public void SetAttackAreaEnable(bool enable) => _AttackAreaChecking = enable;

	public void PlayBlockParticle(float floorHeight)
	{
		Vector3 attackStart = m_AttackAreaCheck_Start.position;
		Vector3 attackEnd = m_AttackAreaCheck_End.position;
		Vector3 position = (attackStart + attackEnd) * 0.5f;

		VisualEffect blockEffect = Instantiate(
			m_BlockParticlePrefab, position, transform.rotation);

		blockEffect.SetFloat("FloorHeight", floorHeight);

		Destroy(blockEffect.gameObject, 3.0f);
	}

#if UNITY_EDITOR
	private void OnDrawGizmos()
	{
		// 공격 영역 그리기
		if (_AttackAreaChecking)
			PhysicsExt.DrawGizmoSpheres(in _AttackAreaCheckGizmoInfo);
	}
#endif

}

