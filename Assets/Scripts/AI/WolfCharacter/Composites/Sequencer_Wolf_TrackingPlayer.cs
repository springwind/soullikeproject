using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequencer_Wolf_TrackingPlayer : BehaviorSequencer
{

	/// <summary>
	/// 공격 요청되었음을 나타내는 키 이름
	/// </summary>
	private string _AttackRequestedKey;

	public Sequencer_Wolf_TrackingPlayer(string attackRequestedKey)
	{
		_AttackRequestedKey = attackRequestedKey;
	}

	public override bool OnRunnableInitialized(BehaviorController behaviorController)
	{
		// 공격 요청을 받은 경우 초기화 실패
		if (behaviorController.GetKeyAsValue<bool>(_AttackRequestedKey)) return false;

		// 캐릭터 감지 서비스
		AddService(() => new BS_CheckingAttackable(
			_AttackRequestedKey,
			2.0f, Constants.LAYER_PLAYERCHARACTER));



		// 잠시 대기
		AddBehavior(() => new BT_WaitSeconds(0.5f));

		// 플레이어 위치를 얻습니다.
		AddBehavior(() => new BT_GetPlayerPosition(
			BehaviorController_Enemy.KEYNAME_MOVETARGETPOSITION,
			BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER,
			BehaviorController_Enemy.KEYNAME_MAXTRACKINGDISTANCE));

		// 플레이어 위치로 이동
		AddBehavior(() => new BT_MoveTo(
			BehaviorController_Enemy.KEYNAME_MOVETARGETPOSITION,
			false));

		return base.OnRunnableInitialized(behaviorController);
	}
}
