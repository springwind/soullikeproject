using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BS_CheckingAttackable : BehaviorService
{
	/// <summary>
	/// 공격 요청을 위한 키 이름
	/// </summary>
	private string _AttackRequestedKey;

	/// <summary>
	/// 감지 반경
	/// </summary>
	private float _DetectRadius;

	/// <summary>
	/// 감지시킬 오브젝트 레이어
	/// </summary>
	private int _DetectObjectLayer;

	#region DEBUG
	private DrawGizmoSphereInfo _DebugGizmoAttackCheckArea;
	#endregion

	public BS_CheckingAttackable(string attackRequestedKey, float detectRadius, int detectObjectLayer)
	{
		_AttackRequestedKey = attackRequestedKey;
		_DetectRadius = detectRadius;
		_DetectObjectLayer = detectObjectLayer;
	}


	public override void ServiceTick()
	{


		Collider[] detectedCollisions = PhysicsExt.OverlapSphere(
			out _DebugGizmoAttackCheckArea,
			behaviorController.transform.position + Vector3.up * 1.21f,
			_DetectRadius,
			_DetectObjectLayer);

		// 감지한 경우
		if (detectedCollisions.Length > 0)
		{
			// 공격 요청
			behaviorController.SetKey(_AttackRequestedKey, true);

		}
	}

	public override void OnDrawGizmos()
	{
		base.OnDrawGizmos();

		PhysicsExt.DrawOverlapSphere(in _DebugGizmoAttackCheckArea);
	}
}
