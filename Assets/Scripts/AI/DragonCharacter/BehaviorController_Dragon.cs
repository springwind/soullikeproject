 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviorController_Dragon : BehaviorController_Enemy
{

	/// <summary>
	/// 플레이어 캐릭터와의 거리를 나타내기 위한 키
	/// </summary>
	public const string KEYNAME_DISTANCETOPLAYER = "DistanceToPlayer";


	protected override void Awake()
	{
		base.Awake();

		AddKey(KEYNAME_DISTANCETOPLAYER, 9999.0f);
	}

	public override void StartBehavior()
	{
		StartBehavior<RootSelector_Dragon>();
	}
}
