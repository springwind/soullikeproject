using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatePanel : MonoBehaviour
{
	[Header("# Hp 수치 UI")]
	public TMP_Text m_HpValueText;
	public Image m_HpValueImage;

	/// <summary>
	/// 최대 체력
	/// </summary>
	private float _MaxHp;

	/// <summary>
	/// 플레이어 상태 패널을 초기화합니다.
	/// </summary>
	/// <param name="maxHp">최대 체력을 전달합니다.</param>
	public void InitializePlayerStatePanel(float maxHp)
	{
		_MaxHp = maxHp;
		UpdateHpValue(_MaxHp);
	}

	/// <summary>
	/// Hp 수치를 갱신합니다.
	/// </summary>
	/// <param name="currentHpValue">현재 체력을 전달합니다.</param>
	public void UpdateHpValue(float currentHpValue)
	{
		// 텍스트 내용 갱신
		m_HpValueText.text = $"{currentHpValue}/{_MaxHp}";

		// 이미지 채움 설정
		m_HpValueImage.fillAmount = currentHpValue / _MaxHp;
	}
}
