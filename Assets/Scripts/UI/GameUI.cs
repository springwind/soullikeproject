using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
	/// <summary>
	/// 플레이어 UI 패널
	/// </summary>
	public PlayerUIPanel playerUIPanel { get; private set; }

	/// <summary>
	/// 적 HP UI
	/// </summary>
	public EnemyHpUI enemyHpUI { get; private set; }

	private void Awake()
	{
		playerUIPanel = GetComponentInChildren<PlayerUIPanel>();
		enemyHpUI = GetComponentInChildren<EnemyHpUI>();
	}


}
