using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;


/// <summary>
/// AI Behavior Controller 를 사용하는 객체의 등록된 키를 노출시키기 위한 창 클래스입니다.
/// </summary>
public class AIBehaviorControllerKeyViewer : EditorWindow
{
	/// <summary>
	/// 현재 스크롤된 위치를 기록하기 위한 변수
	/// </summary>
	private Vector2 _CurrentScrollPosition;

	/// <summary>
	/// 선택된 BehaviorController 객체들을 나타냅니다.
	/// </summary>
	private List<BehaviorController> _BehaviorControllers = new();

	[MenuItem("Window/AI/BehaviorControllerKeyViewer")]
	private static void Init()
	{
		// 창을 띄웁니다.
		AIBehaviorControllerKeyViewer window = GetWindow<AIBehaviorControllerKeyViewer>();

		// 띄운 창 크기를 설정합니다.
		window.minSize = new Vector2(200.0f, 200.0f);
	}

	private void Update()
	{
		// 선택된 모든 오브젝트 트랜스폼을 얻습니다.
		Transform[] selectedTransformsInWorld = Selection.GetTransforms(
			SelectionMode.TopLevel | SelectionMode.ExcludePrefab | SelectionMode.Editable);

		// 선택된 BehaviorController 객체들을 얻어 리스트에 추가합니다.
		_BehaviorControllers.Clear();

		foreach (Transform selectedTransform in selectedTransformsInWorld)
		{
			// 선택된 Transform에 추가된 BehaviorController 컴포넌트를 얻습니다.
			BehaviorController behaviorController = 
				selectedTransform.GetComponent<BehaviorController>();
			if (behaviorController != null)
			{
				_BehaviorControllers.Add(behaviorController);
			}
		}

		// 창을 갱신합니다.
		Repaint();
	}

	private void OnGUI()
	{
		try
		{
			// 스크롤 시작 위치 설정
			_CurrentScrollPosition = GUILayout.BeginScrollView(_CurrentScrollPosition, false, true);

			// 게임 오브젝트 이름 표시용 스타일
			GUIStyle labelStyle_GameObjectName = new();
			labelStyle_GameObjectName.fontSize = 15;
			labelStyle_GameObjectName.fontStyle = FontStyle.Bold;
			labelStyle_GameObjectName.normal.textColor = Color.yellow;


			// 선택된 행동 객체를 모두 확인합니다.
			foreach (BehaviorController behaviorController in _BehaviorControllers)
			{
				// 선택된 게임 오브젝트 이름 출력
				GUILayout.Label(
					behaviorController.gameObject.name,
					labelStyle_GameObjectName);

				// 키와 값을 이 곳에서 출력합니다.
				foreach (KeyValuePair<string, object> keyInfo in behaviorController.keys)
				{
					string keyName = keyInfo.Key;
					object keyValue = keyInfo.Value;

					string valueIsNullString = keyValue == null ? "▷" : "▶";
					string keyNameString = $"[ {keyName} ]";
					string keyValueString = keyValue == null ?
						"- -" : keyValue.ToString();

					GUILayout.Label($"{valueIsNullString} {keyNameString} {keyValueString}");
				}

				GUILayout.Space(20.0f);
			}
		}
		catch (System.Exception) 
		{
			_BehaviorControllers.Clear();
		}

		// 스크롤 끝 위치 설정
		GUILayout.EndScrollView();
	}
}
#endif